#!/bin/bash

save_path=$1

curl --location --header "PRIVATE-TOKEN: $api_token" "https://gitlab.com/api/v4/projects/$proj_id/jobs/artifacts/dev/raw/build.log?job=build_dev" -o $save_path
